version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Adaptive Archsimkats Valley",
    description = "A gorgeous valley is turned upside down into a battle ground. Map made by (Jip) Willem Bart Wijnia. Thanks to Archimiskat and the community for their input. You can view the latest version on: https://gitlab.com/w.b.wijnia/archimiskats-valley",
    preview = '',
    map_version = 3,
    AdaptiveMap = true,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {28158.82, 167863.9},
    map = '/maps/Adaptive_Archsimkats_Valley.v0003/Adaptive_Archsimkats_Valley.scmap',
    save = '/maps/Adaptive_Archsimkats_Valley.v0003/Adaptive_Archsimkats_Valley_save.lua',
    script = '/maps/Adaptive_Archsimkats_Valley.v0003/Adaptive_Archsimkats_Valley_script.lua',
    norushradius = 100,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3', 'ARMY_4'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTAL_CIVILIAN JIP' ),
            },
        },
    },
}
