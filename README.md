# Archsimkats Valley

A map for Supreme Commander: Forged Alliance

![](/images/impression-1.png)

# Trailer

You can find the trailer at:
 - https://youtu.be/S3fvUrhhMsE

# Statistics

Mass reclaim:    23,781.22
Energy reclaim: 167,863.90
Reclaim time:   218,600.00

There are various reclaim hot spots on the map to assist players in scavenging the map and therefore contesting for map control.

![](/images/impression-2.png)

# Design

The map is designed in collaboration with Archsimkat, a high-ranked ladder player in the Forged Alliance Forever (FAF) community. All the mass extractors are placed such that they make for clear primary and secondary expansions. In turn the map is easier to understand yet complicated because of the various approaches that a player can take.

![](/images/impression-3.png)

# Two versions

There are two versions of the map:
 - maps/Adaptive Archsimkats Valley
 - mapsArchsimkats Valley

The former is for the FAF community, where as the latter is for the LOUD community. The balance in the LOUD community requires more space between players and therefore the map is stand-off between two players. By default the map is a 2v2 for FAF.

![](/images/impression-4.png)

# Installation

For the FAF-version store the map in the following folder:
 - %USER%/My Documents/My Games/Gas Powered Games/Supreme Commander Forged Alliance/maps

For the LOUD-version store the map in the following folder:
 - %STEAM%/userapps/common/Supreme Commander Forged Alliance/LOUD/usermaps

If the map folder does not exist in either situation then you can freely make one with the corresponding name.

# License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All other assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

