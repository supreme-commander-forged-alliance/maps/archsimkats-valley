version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Archsimkats Valley",
    description = "A version of ArchsimKats Valley for the LOUD community. Built for 1v1",
    preview = '',
    map_version = 1,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {29006.68, 167858},
    map = '/maps/Archsimkats Valley/Archsimkats_Valley.scmap',
    save = '/maps/Archsimkats Valley/Archsimkats_Valley_save.lua',
    script = '/maps/Archsimkats Valley/Archsimkats_Valley_script.lua',
    norushradius = 60,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_2', 'ARMY_1'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'Jip Archsimkat NEUTRAL_CIVILIAN ARMY_17' ),
            },
        },
    },
}
