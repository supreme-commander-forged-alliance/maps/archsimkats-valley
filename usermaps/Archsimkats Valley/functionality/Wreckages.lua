
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')

local EffectUtilities = import('/lua/EffectUtilities.lua')
local EffectTemplates = import('/lua/EffectTemplates.lua')

do

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- spawn wreckage for all armies

    local props = { }
    local armies = ListArmies()

    for k, army in armies do 

        -- check if there is such a group
        local wreckageGroup = ScenarioUtils.FindUnitGroup('Wrecks', ScenarioInfo.Env.Scenario.Armies[army].Units)
        if wreckageGroup then

            -- if so, spawn it and turn it into a wreck
            local platoonList, tblResult, treeResult = ScenarioUtils.CreatePlatoons(army, wreckageGroup )
            for num,unit in tblResult do
                local prop = unit:CreateWreckageProp(0)
                unit:Destroy()
                table.insert(props, prop)
            end
        end
    end

    ------------------ ------------------ ------------------ ------------------ ------------------                              
    -- add some additional particle effects to the wreckages

    local effects = {
        -- EffectTemplates.DamageFire01,
        -- EffectTemplates.DamageFireSmoke01,
        EffectTemplates.TreeBurning01,
        EffectTemplates.TreeBurning01,
        -- EffectTemplates.op_fire_01,
        -- EffectTemplates.DamageStructureFire01,
        EffectTemplates.DamageStructureFireSmoke01,
    }

    for k, prop in props do

        local apply = Random()
        
        if apply > 0.25 then
        
            -- get a random bone
            local indexBone = math.floor(Random() * prop:GetBoneCount())
            local bone = prop:GetBoneName(indexBone)

            -- determine the emitter and attach
            local indexEmitter = math.floor(Random() * table.getn(effects)) + 1
            local emitters = EffectUtilities.CreateBoneEffects(prop, bone, "ARMY_17", effects[indexEmitter]) or false

            if emitters then
            
                -- ensure that if prop is destroyed, so is the emitter
                for k, emitter in emitters do 
                    prop.Trash:Add(emitter)
                end
                
            end
        end
    end

end