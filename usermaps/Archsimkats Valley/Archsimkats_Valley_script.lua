local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

function OnPopulate()
    ScenarioUtils.InitializeArmies()
end

function OnStart(scenario)

	-- spawn the trucks and start their behavior
	doscript("/maps/Archsimkats Valley/functionality/Wreckages.lua")

end
